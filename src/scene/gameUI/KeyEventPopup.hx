package scene.gameUI;

import haxe.MainLoop.MainEvent;
import hxd.res.DefaultFont;
import dual.pyke.Key;
import dual.Pyke;
import hxd.Event;
import h2d.Text;
import h2d.Tile;
import h2d.Font;
import h2d.Flow;
import dual.tools.ScreenTool as Screen;


class KeyEventPopup extends Flow{

    public var label:Text;
    public var targetPyke:Pyke;
    private var callback:Int->Void;


    public function new(targetPyke:Pyke, text:String = "", callback:Int->Void) { // callback that handle the attribution of the item
        super();
        this.callback = callback;
        this.targetPyke = targetPyke;
        label = new h2d.Text(DefaultFont.get());
        label.textColor = 0xfefefe;
        label.textAlign = Left;
        this.backgroundTile = Tile.fromColor(0x333333);
        padding = 15;
        constraintWidth = Screen.intw(0.4);
        label.text = text;
        label.smooth = true;
        addChild(label);
        
        Game.currentScene.addEventListener(onEvent);

    }

    function onEvent(event : Event) {
        switch(event.kind) {
            case EKeyUp: onKeyEvent(event);
            default:
        }
    }
    public function onKeyEvent(event : Event) {
        var i = 0;
        for (k in targetPyke.keyBind.itemKey) {
            if (k == event.keyCode) {
                Game.currentScene.removeEventListener(onEvent);
                callback(i);
            }
            i++;
        }
            
    }
        
}
