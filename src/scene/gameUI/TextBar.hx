package scene.gameUI;


import hxd.Window;
import h2d.Flow;
import h2d.Text;
import h2d.Font;
import h2d.Tile;

class TextBar extends Flow{

    public var label:Text;
    

    /**
     * create text bar for the map selection menu
     * @param font used font
     * @param text text displayed
     * @param background background tile displayed
     */

    public function new(font:Font, text:String = "", ?background:Tile) {
        super();
        
        if (background == null) {
            
            this.backgroundTile = h2d.Tile.fromColor(0x1d1d1d);
            
        } else {
            this.backgroundTile = background;
        }
        
        
        label = new h2d.Text(font);
        label.textColor = 0xfefefe;
        label.textAlign = Left;
    
        padding = 15;
        minWidth = Std.int(0.5 * Window.getInstance().width);
        fillWidth = true;
        label.text = text;
        label.smooth = true;
        this.background.blendMode = Alpha;
        this.background.smooth = true;
        addChild(label);
        enableInteractive = true;
		interactive.cursor = Button;
        interactive.onOver = function( e : hxd.Event ) {
            this.label.textColor = 0xfe0000;
            
            
        }
        this.interactive.onOut = function( e : hxd.Event ) {
            this.label.textColor = 0xfefefe;
            
        }

    }

    

    
}