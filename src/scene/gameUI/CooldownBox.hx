package scene.gameUI;

import dual.pyke.Cooldown;
import pattern.Observer.NotifPack;
import hxd.Math;
import dual.pyke.Spell;
import dual.Pyke;
import h2d.Tile;
import hxd.Res;
import h2d.Object;
import h2d.Font;
import h2d.Text;
import h2d.Bitmap;
import h2d.Flow;
import pattern.Observer;

using pattern.Observer.Observables;


/**
 * A button with centered text and bitmap background
 */

class CooldownBox extends Object {

    public var background:Bitmap;
    public var label:Text;

    public function new(color:Int, ?parent:Object) {
        super(parent);
        var tile = Tile.fromColor(color,50,50,0.1);
        background = new Bitmap(tile,this);
        label = new Text(FontFolder.pirata16,this);
        label.textAlign = Center;
        label.x = 10;
        label.y = 25;
    }

}

class CooldownBoxContainer extends Flow {

    public var grabBox:CooldownBox;
    public var ghostBox:CooldownBox;
    public var dashBox:CooldownBox;
    public var ultBox:CooldownBox;
    public var flashBox:CooldownBox;


    public function new(pyke:Pyke) {
        super();
        padding = 5;
        horizontalSpacing= 15;
        grabBox = new CooldownBox(0x00ff00,this);
        ghostBox = new CooldownBox(0x0000ff,this);
        dashBox = new CooldownBox(0xffa500,this);
        ultBox = new CooldownBox(0xff0000,this);
        flashBox = new CooldownBox(0xffff00,this);
        grabBox.label.text = Std.string(Math.round(pyke.cooldown.grabTime*10)*0.1);
        ghostBox.label.text = Std.string(Math.round(pyke.cooldown.ghostTime*10)*0.1);
        dashBox.label.text = Std.string(Math.round(pyke.cooldown.dashTime*10)*0.1);
        ultBox.label.text = Std.string(Math.round(pyke.cooldown.ultTime*10)*0.1);
        flashBox.label.text = Std.string(Math.round(pyke.cooldown.flashTime*10)*0.1);

        var callBack = function(notif:NotifPack):Any {
            if (notif.type == Spell.grab) {
                grabBox.label.text = Std.string(Math.round(cast(notif.data, Float)*10)*0.1);
            } else if (notif.type == Spell.ghost) {
                ghostBox.label.text = Std.string(Math.round(cast(notif.data, Float)*10)*0.1);
            } else if (notif.type == Spell.dash) {
                dashBox.label.text = Std.string(Math.round(cast(notif.data, Float)*10)*0.1);
            } else if (notif.type == Spell.ult) {
                ultBox.label.text = Std.string(Math.round(cast(notif.data, Float)*10)*0.1);
            } else if (notif.type == Spell.flash) {
                flashBox.label.text = Std.string(Math.round(cast(notif.data, Float)*10)*0.1);
            }
               
            return null;
        }
        var w = new Watcher("pykeCd", callBack);
        pyke.cooldown.addObserver(w);

    }
}
