package scene.gameUI;

import h2d.filter.Blur;
import h2d.filter.Bloom;
import h2d.col.Point;
import h3d.mat.PbrMaterial.PbrProps;
import h3d.mat.Texture;
import h2d.Particles.ParticleGroup;
import h2d.Particles;
import motion.Actuate;
import h3d.Vector;
import scene.sound.SoundManager;
import pattern.Observer.NotifPack;
import h2d.filter.DropShadow;
import h2d.filter.ColorMatrix;
import h2d.filter.Glow;
import h2d.filter.Group;
import h3d.Matrix;
import dual.pyke.Item.ItemFactory;
import dual.Pyke;
import h2d.Slider;
import h2d.Flow;
import h2d.Object;
import h2d.Tile;
import hxd.res.DefaultFont;
import hxd.Window;

using pattern.Observer.Observables;

class ScrollBox extends ScrollArea {
    
    public var flowBox:Flow;
    public var slider:Slider;
    public var selected:Int=1;
    public var boxInView:Int;
    public var pyke:Pyke;
    public var selectingSlot:Bool = false;

    public function new(w:Int, h:Int, boxInView:Int, infoList:Array<Flow>, pyke:Pyke) {
        super(w, h);
        this.pyke = pyke;
        this.boxInView = boxInView;
        flowBox = new h2d.Flow(this);
		flowBox.layout = Vertical;
		flowBox.verticalSpacing = 5;
		flowBox.padding = 5;
        flowBox.minWidth = w;
        flowBox.minHeight = h;
        flowBox.maxHeight = h;
        flowBox.constraintSize(w,h);
        flowBox.fillHeight = true;
        flowBox.backgroundTile = Tile.fromColor(0x5d5d5d);
        flowBox.backgroundTile.scaleToSize(flowBox.outerWidth, flowBox.outerHeight);
        for (info in infoList) {
            flowBox.addChild(info);
        }
        applyFilter();

        

    }

    public function update(dt:Float) {
        var key = pyke.keyBind;
        key.update(dt);
        if (!selectingSlot) {
            if (key.ghost) scroll(-1);

            if (key.swapDirection) scroll(1);

            if (key.ult) buy();
        }
    }

    public function scroll(direction:Int) {
        if (direction == -1) {
            if (selected == 1) { //0 is a scaleGrid of the flow element
                if (boxInView < flowBox.numChildren) {
                    var temp = flowBox.getChildAt(flowBox.numChildren-1);
                    flowBox.removeChild(temp);
                    flowBox.addChildAt(temp,0);
                }
            } else {
                selected--;
            }
        } else { //direction == 1
            if (selected == boxInView ) {
                if (boxInView < flowBox.numChildren) {
                    var temp = flowBox.getChildAt(1);
                    flowBox.removeChild(temp);
                    flowBox.addChild(temp);
                }
            } else if (selected < flowBox.numChildren - 1) {
               selected++;
            }
        }
        /*if (selected+direction < 1) {
            var temp = flowBox.getChildAt(flowBox.numChildren-1);
            flowBox.removeChild(temp);
            flowBox.addChildAt(temp,1);
        } else if (selected+direction >= boxInView) {
            var temp = flowBox.getChildAt(1);
            flowBox.removeChild(temp);
            flowBox.addChild(temp);
        } else {
            selected+=direction;
        }*/
        
        applyFilter();

    }

    public function buy() {
        //add gold check
        var selectedItemID = cast(flowBox.getChildAt(selected), ItemCard).itemID;
        if ( pyke.gold >= ItemFactory.getItemInfo(selectedItemID).value && !pyke.isFull() ) {
            pyke.gold -= ItemFactory.getItemInfo(selectedItemID).value;
            var selectedItem = ItemFactory.create(selectedItemID);
            var popup:KeyEventPopup;
            selectingSlot = true;
            var callback = function (pos:Int) {
                pyke.give(selectedItem, pos);
                popup.remove();
                popup=null;
                /*var soundFX = if( hxd.res.Sound.supportedFormat(Mp3) || hxd.res.Sound.supportedFormat(OggVorbis) ) hxd.Res.sound.shop.buy else null;
                if (soundFX != null) {
                    SoundManager.getInstance().trigger(soundFX);
                }*/
                selectingSlot = false;
            }
            popup = new KeyEventPopup(pyke, "Choose a item slot for your item", callback);
            popup.x = localToGlobal(new Point(width*0.5,height*0.5)).x-0.5*popup.innerWidth;
            popup.y = localToGlobal(new Point(width*0.5,height*0.5)).y;
            Game.currentScene.addChild(popup);
            
            
        //TODO ability to sell item
        //TODO restrict the number of item to 6
        //show pop up to assign the item to a slot

            
        } else {
            /*var soundFX = if( hxd.res.Sound.supportedFormat(Mp3) || hxd.res.Sound.supportedFormat(OggVorbis) ) hxd.Res.sound.shop.noMoney else null;
            if (soundFX != null) {
                SoundManager.getInstance().trigger(soundFX);
            }*/
            Actuate.update(cast(flowBox.getChildAt(selected), ItemCard).wrong, 0.05, [0], [0.3]).repeat(10).reflect().onComplete(function () {
                cast(flowBox.getChildAt(selected), ItemCard).filter = null;
            });
        }
        
    }

    public function applyFilter() {
        var i=0;
        for (info in flowBox) {
            if (i==0) {
                i++;//skip the scaleGrid
                continue;
            }
            cast(info, ItemCard).filter = null;
        }
        var m = new Matrix();
        m.identity();
		m.colorLightness(-0.05);
        this.notify(new NotifPack(cast(flowBox.getChildAt(selected), ItemCard).itemID));
        cast(flowBox.getChildAt(selected), ItemCard).filter = new Group([ new ColorMatrix(m) ]);// ]
    }


}