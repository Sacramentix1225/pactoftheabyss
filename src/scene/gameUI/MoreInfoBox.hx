package scene.gameUI;


import dual.pyke.Item;
import hxd.res.DefaultFont;
import h2d.Object;
import h2d.Bitmap;
import hxd.Window;
import h2d.Flow;
import h2d.Text;
import h2d.Font;
import h2d.Tile;

class MoreInfoBox extends Flow { 

    public var nameTag:Text;
    private var firstBlock:Flow;
    public var stat:Text;
    public var effectDescription:Text;
    public var lore:Text;
    public var logo:Bitmap;
    //public var itemID:Int;
    

    /**
     * create text bar for the map selection menu
     * @param font used font
     * @param text text displayed
     * @param background background tile displayed
     */

    public function new(w:Float, h:Float, itemID:Int, ?parent:Object) {
        super(parent);
        //this.itemID = itemID;
        var item = ItemFactory.create(itemID);

        
        padding = 7;
        borderWidth=0;
        fillWidth = true;
        layout = Vertical;
        minHeight = Std.int(h);
        constraintSize(w,h);
        verticalSpacing = 15;

        logo = new Bitmap(Tile.fromColor(Std.random(16777215), 32, 32));

        backgroundTile = Tile.fromColor(0x5d5d5d);

        nameTag = new h2d.Text(DefaultFont.get());
        nameTag.textColor = 0xfefefe;
        nameTag.textAlign = Center;
        nameTag.text = ItemFactory.getItemInfo(itemID).name;

        stat = new h2d.Text(DefaultFont.get());
        stat.textColor = 0xfefefe;
        stat.textAlign = Left;
        stat.text = "Stat : " + ItemFactory.getItemInfo(itemID).stat;

        effectDescription = new h2d.Text(DefaultFont.get());
        effectDescription.textColor = 0xfefefe;
        effectDescription.textAlign = Left;
        effectDescription.text = "Effect : " + ItemFactory.getItemInfo(itemID).effect;

        lore = new h2d.Text(DefaultFont.get());
        lore.textColor = 0xfefefe;
        lore.textAlign = Left;
        lore.text = "Lore : " + ItemFactory.getItemInfo(itemID).lore;

        //this.background.blendMode = Alpha;
        //this.background.smooth = true;
        var contentHeight = 0.0;

        firstBlock = new Flow();
        firstBlock.constraintSize(w, h);
        firstBlock.addChild(logo);
        firstBlock.addChild(nameTag);
        addChild(firstBlock);
        addChild(stat);
        addChild(effectDescription);
        addChild(lore);
        contentHeight += firstBlock.outerHeight;
        contentHeight += stat.textHeight;
        contentHeight += effectDescription.textHeight;
        contentHeight += lore.textHeight;
        verticalSpacing = Std.int((innerHeight-contentHeight)/ (this.numChildren-1));
        

    }

    public function onSelectionChange(itemID) {
        nameTag.text = ItemFactory.getItemInfo(itemID).name;
        stat.text = "Stat : " + ItemFactory.getItemInfo(itemID).stat;
        effectDescription.text = "Effect : " + ItemFactory.getItemInfo(itemID).effect;
        lore.text = "Lore : " + ItemFactory.getItemInfo(itemID).lore;
        var contentHeight = 0.0;
        contentHeight += firstBlock.outerHeight;
        contentHeight += stat.textHeight;
        contentHeight += effectDescription.textHeight;
        contentHeight += lore.textHeight;
        verticalSpacing = Std.int((innerHeight-contentHeight)/ (this.numChildren-1));

    }

    

    
}