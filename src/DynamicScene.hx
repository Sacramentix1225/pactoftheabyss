import h2d.Scene;

interface SceneEvent   {
    public function update(dt:Float):Void;
    public function onResize():Void;

}

//abstract
class DynamicScene extends Scene implements SceneEvent  {
    public function update(dt:Float):Void {
        
    }
    public function onResize():Void {

    }
    public function new() {
        super();
    }
}
