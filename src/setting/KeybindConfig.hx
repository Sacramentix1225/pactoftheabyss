package setting;
#if !js
import sys.io.FileInput;
import sys.io.FileOutput;
import sys.FileSystem;
import sys.io.File;
import haxe.io.BytesOutput;
import haxe.io.Bytes;
#end
typedef Keybind = {
        var topPyke:PlayerKeybind;
        var botPyke:PlayerKeybind;
        var pause:Int;


    }

    typedef PlayerKeybind = {

        var swapDirectionKey:Int;
        var grabKey:Int;
        var ghostKey:Int;
        var dashKey:Int;
        var ultKey:Int;
        var flashKey:Int;
        var item0Key:Int;
        var item1Key:Int;
        var item2Key:Int;
        var item3Key:Int;
        var item4Key:Int;
        var item5Key:Int;

    }


class KeybindConfig {


    public static var list:Array<Keybind>;
    public static var currentKeybindID:Int = 0;
#if !js
    public static function fromFolder() {
        list = new Array<Keybind>();
        for (file in FileSystem.readDirectory("keybind")) {
			var json = File.read("keybind\\" + file);
            var data = json.readAll();
            var keybind:Keybind = haxe.Json.parse(data.getString(0,data.length));
            list.push(keybind);
        }
    }
#end

#if js
    public static function fromFolder() {
        list = new Array<Keybind>();
        var playerkey:PlayerKeybind = {
            dashKey:99,
            flashKey:110,
            ghostKey:98,
            grabKey:97,
            item0Key:103,
            item1Key:104,
            item2Key:105,
            item3Key:100,
            item4Key:101,
            item5Key:102,
            swapDirectionKey:96,
            ultKey:108
        };
        var keybind:Keybind = {
            topPyke:playerkey,
            botPyke:playerkey,
            pause:27
        };
        list.push(keybind);
    }
#end

    public static function getCurrent():Keybind {
        return list[currentKeybindID];
    }


}
