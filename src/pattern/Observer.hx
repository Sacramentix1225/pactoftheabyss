package pattern;



interface Observer {
    public function notified(sender:{}, ?data:NotifPack) : Void;
    public var type:String;
}

class Observables {
  static public var observables:Map<{}, Array<Observer>> = new Map();
  
  static public function addObserver(subject:{}, observer:Observer)
  {
    if(!observables.exists(subject))
      observables[subject] = new Array<Observer>();
    observables[subject].push(observer);
  }
  
  static public function notify(subject:{}, ?data:NotifPack)
  {
    if(observables.exists(subject))
      for(obs in observables[subject])
        obs.notified(subject, data);
  }
}

class Watcher implements Observer {
    public var type:String;
    public function new(type:String, callback:NotifPack->Any) {
        this.type = type;
        onNotif = callback;
    }
    public dynamic function onNotif(para:NotifPack):Any {
        return null;
	  }

    public function notified(sender:{}, ?data:NotifPack) {
        onNotif(data);
    }
}

class NotifPack {
	public var type:Int;
	public var data:Any;
	public function new(data:Any, ?type:Int) {
		this.data = data;
		this.type = type;
	}
}

