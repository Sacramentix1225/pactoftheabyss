package vfx.particle;

import h2d.col.Point;
import h3d.mat.Texture;
import h2d.Object;
import h2d.Particles;
import hxd.Window;
import h3d.Vector;

class FlashFx { 

    public static function depart(?parent:Object, pos:Point):Particles {
        var particles = new Particles(parent);
		var g = new ParticleGroup(particles);
        g.sizeRand = .2;
        g.size = 2;
		g.life = 0.4;
        g.emitStartDist = 5;
        g.nparts = 2500;
        g.animationRepeat = 0;
        g.isRelative = false;
        g.emitLoop = false;
        g.speedRand = .2;
		g.speed = 100;
        
        //g.speedIncr = -10;
        g.texture = Texture.fromColor(0xffff33, 0.2);
		g.rotSpeed = 2;
		g.emitMode = PartEmitMode.Point;
        g.emitDist = 20;
		g.dx = Std.int(pos.x);
		g.dy = Std.int(pos.y);
        g.fadeIn = 0;
		particles.addGroup(g);
        
        particles.onEnd = function () {
            g.enable = false;

        };
        
        return particles;
        
    }

    public static function onPyke(?parent:Object):Particles {
        var particles = new Particles(parent);
		var g = new ParticleGroup(particles);
        g.sizeRand = .2;
        g.size = 2;
		g.life = 0.2;
        g.emitStartDist = 5;
        g.nparts = 1000;
        g.animationRepeat = 0;
        g.isRelative = true;
        g.emitLoop = false;
        g.speedRand = .2;
		g.speed = 100;
        g.fadeIn = 0;
        
        //g.speedIncr = -10;
        g.texture = Texture.fromColor(0xffff33, 0.2);
		g.rotSpeed = 2;
		g.emitMode = PartEmitMode.Point;
        g.emitDist = 20;
		g.dx = 0;
		g.dy = 0;
		particles.addGroup(g);
        
        particles.onEnd = function () {
            g.enable = false;

        };
        
        return particles;
        
    }

    
}