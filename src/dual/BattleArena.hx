package dual;

import haxe.MainLoop;
import h2d.Graphics;
import scene.FontFolder;
import h2d.Bitmap;
import hxd.Res;
import h2d.Scene;
import h2d.Text;
import hxd.Window;
import dual.Pyke;
import pattern.Observer;
import scene.gameUI.CooldownBox;
import setting.KeybindConfig;
import dual.tools.ScreenTool as Screen;

using pattern.Observer.Observables;
using dual.Collision;

/**
 * Class to create a screen display of the options menu
 * you can insert all UI element that you want to display. 
 * You can find preBuild UI element in scene.gameUI
 */

class BattleArena extends DynamicScene {
    private var scene:Scene;
    
    public var lastTimeStamp:Float;
    public var updateEvent:MainEvent;

    public var timeFactor:Float = 1;
    public var isControlBlocked:Bool= false;

    private var background:Bitmap;
    public var topPyke:Pyke;
    public var botPyke:Pyke;
    public var topHP:Text;
    public var topGold:Text;
    public var botHP:Text;
    public var botGold:Text;
    public var topCd:CooldownBoxContainer;
    public var botCd:CooldownBoxContainer;
    public var configID:Int;
    #if debug
    public var topBody:Graphics;
    public var topHarpoon:Graphics;
    public var botBody:Graphics;
    public var botHarpoon:Graphics;
    #end
    public var fps:Text;

    private static var instance:BattleArena;

    public var debugTopBody:Graphics;
    public var debugTopGrab:Graphics;
    public var debugBotBody:Graphics;
    public var debugBotGrab:Graphics;

    public static function t(t:Float):Float {
        return t/BattleArena.getInstance().timeFactor;
    }

    public static function getInstance():BattleArena {
        if (instance==null) instance = new BattleArena();
        return instance;
    }
    
    /**
     * Create a new scene of the options menu
     * if you want to display it don't forget to change the current scene of the game with Game.instance.setScene( insert the new scene )
     */

    private function new() {
        super();


        var tile = Res.background.battleArena.toTile();
        tile.scaleToSize(Screen.w(), Screen.h());
        var keybind = KeybindConfig.getCurrent();
        var topItemKey = new Array<Int>();
        topItemKey.push(keybind.topPyke.item0Key);
        topItemKey.push(keybind.topPyke.item1Key);
        topItemKey.push(keybind.topPyke.item2Key);
        topItemKey.push(keybind.topPyke.item3Key);
        topItemKey.push(keybind.topPyke.item4Key);
        topItemKey.push(keybind.topPyke.item5Key);
        var botItemKey = new Array<Int>();
        botItemKey.push(keybind.botPyke.item0Key);
        botItemKey.push(keybind.botPyke.item1Key);
        botItemKey.push(keybind.botPyke.item2Key);
        botItemKey.push(keybind.botPyke.item3Key);
        botItemKey.push(keybind.botPyke.item4Key);
        botItemKey.push(keybind.botPyke.item5Key);
        var topKeyBind = new dual.pyke.Key(keybind.topPyke.swapDirectionKey, keybind.topPyke.grabKey, keybind.topPyke.ghostKey, keybind.topPyke.dashKey, keybind.topPyke.ultKey, keybind.topPyke.flashKey, topItemKey);
        var botKeyBind = new dual.pyke.Key(keybind.botPyke.swapDirectionKey, keybind.botPyke.grabKey, keybind.botPyke.ghostKey, keybind.botPyke.dashKey, keybind.botPyke.ultKey, keybind.botPyke.flashKey, botItemKey);

        background = new Bitmap(tile);
        var generatedPyke = Pyke.fromConfig(topKeyBind, botKeyBind);
        topPyke = generatedPyke[0];
        topPyke.y = Screen.h(0.05);
        topPyke.x = Screen.w(0.5);
        botPyke = generatedPyke[1];
    
        botPyke.y = Screen.h(0.95);
        botPyke.x = Screen.w(0.5);
        botPyke.rotate(3.1415);

        topHP = new Text(FontFolder.pirata16);
        topHP.x = 5;
        topHP.y = 5;
        topHP.text = topPyke.hp+"/"+topPyke.stat.maxHP;
        topHP.textColor = 0xff0000;
        var callBack = function(notif:NotifPack):Any {
                if (notif.type == Pyke.hpID) topHP.text = Std.string(notif.data + " / " + topPyke.stat.maxHP);
                return null;
            }
        var hpw1 = new Watcher("HP", callBack);
        topPyke.addObserver(hpw1);


        topGold = new Text(FontFolder.pirata16);
        topGold.x = Screen.w() -5;
        topGold.textAlign = Right;
        topGold.y = 5;
        topGold.text = Std.string(topPyke.gold);
        topGold.textColor = 0xd4af37;
        var callBack = function(notif:NotifPack):Any {
                if (notif.type == Pyke.goldID) topGold.text = Std.string(notif.data);
                return null;
            }
        var goldw1 = new Watcher("Gold", callBack);
        topPyke.addObserver(goldw1);


        botHP = new Text(FontFolder.pirata16);
        botHP.x = 5;
        botHP.y = Screen.h() - 35;
        botHP.text = botPyke.hp+"/"+botPyke.stat.maxHP;
        botHP.textColor = 0xff0000;
        var callBack = function(notif:NotifPack):Any {
                if (notif.type == Pyke.hpID) botHP.text = Std.string(notif.data + " / " + botPyke.stat.maxHP);
                return null;
            }
        var hpw2 = new Watcher("HP", callBack);
        botPyke.addObserver(hpw2);

        botGold = new Text(FontFolder.pirata16);
        botGold.textAlign = Right;
        botGold.x = Screen.w()-5;
        botGold.y = Screen.h()-5-botGold.textHeight;
        botGold.text = Std.string(botPyke.gold);
        botGold.textColor = 0xd4af37;
        callBack = function(notif:NotifPack):Any {
                if (notif.type == Pyke.goldID) botGold.text = Std.string(notif.data);
                return null;
            }
        var goldw2 = new Watcher("Gold", callBack);
        botPyke.addObserver(goldw2);

        topCd = new CooldownBoxContainer(topPyke);
        topCd.y = 5;
        topCd.x = 50;
        botCd = new CooldownBoxContainer(botPyke);
        botCd.y = Screen.h()-60;
        botCd.x = 50;

        addChild(background);
        addChild(topPyke);
        addChild(botPyke);
        addChild(topHP);
        addChild(botHP);
        addChild(topGold);
        addChild(botGold);
        addChild(topCd);
        addChild(botCd);
        //fps =  new Text(FontFolder.pirata16,this);
        //fps.text = Timer.fps();

        lastTimeStamp = haxe.Timer.stamp();
        updateEvent = MainLoop.add(updatePerTick);


        #if debug
        topBody =  new Graphics(this);
        botBody =  new Graphics(this);
        topHarpoon = new Graphics(this);
        botHarpoon = new Graphics(this);
        #end
        
        
    }

    
    /**
     * code below is executed each time the window is resized ( don't work on js )
     */

    override public function onResize() {
        topPyke.tile.scaleToSize(Screen.h(0.05), Screen.h(0.05));
        botPyke.tile.scaleToSize(Screen.h(0.05), Screen.h(0.05));
        background.tile.scaleToSize(Screen.w(), Screen.h());
        
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */
    
    override public function update(dt:Float) {        
        if (Round.shopInstance != null) {
            Round.shopInstance.update(dt);
        }
        this.x = this.x;

        //  debug hitbox  //
        #if debug
        topBody.clear();
        topBody.beginFill(0xff0000, 0.3);
        var topBounds = topPyke.body.getBounds();
        topBody.drawRect(topBounds.x, topBounds.y, topBounds.width, topBounds.height);
        topBody.endFill();

        botBody.clear();
        botBody.beginFill(0xff0000, 0.3);
        var botBounds = botPyke.body.getBounds();
        botBody.drawRect(botBounds.x, botBounds.y, botBounds.width, botBounds.height);
        botBody.endFill();

        botHarpoon.clear();
        botHarpoon.beginFill(0x0000ff, 0.3);
        var hitbox = botPyke.getHarpoonHitbox();
        for (p in hitbox) {
            botHarpoon.lineTo(p.x, p.y);
        }
        botHarpoon.endFill();

        topHarpoon.clear();
        topHarpoon.beginFill(0x0000ff, 0.3);
        var hitbox = topPyke.getHarpoonHitbox();
        for (p in hitbox) {
            topHarpoon.lineTo(p.x, p.y);
        }
        topHarpoon.endFill();

        #end

    }

    public function updatePerTick() {
        if (!Round.isShopping) {
            var timeStamp = haxe.Timer.stamp();
            var dt = timeStamp-lastTimeStamp;
            lastTimeStamp = timeStamp;
            topPyke.checkHitWith(botPyke);
            botPyke.checkHitWith(topPyke);
        }
    }
    
     
}