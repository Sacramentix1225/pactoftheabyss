package dual.tools;

import h2d.col.Bounds;
import h2d.col.Point;
import h2d.Bitmap;
import h2d.Object;
import hxd.Window;

class ScreenTool {

    public static function isOnScreen(subject:Object):Bool {
        return isPointOnScreen(subject.localToGlobal());
    }

    public static function isOnYScreen(subject:Object):Bool {
        return isPointOnScreen(subject.localToGlobal());
    }

    public static  function isYOnScreen(p:Point):Bool {
        if ( p.y > 0 && p.y < Window.getInstance().height ) return true;
        else return false;
    }

    public static  function isPointOnScreen(p:Point):Bool {
        if ( p.x > 0 && p.x < Window.getInstance().width && p.y > 0 && p.y < Window.getInstance().height ) return true;
        else return false;
    }
    // return pourcent of screen height
    public static inline function h(?pourcent:Float):Float {
        if (pourcent == null) return Window.getInstance().height;
        return Window.getInstance().height*pourcent;
    }
    // return pourcent of screen width
    public static inline function w(?pourcent:Float):Float {
        if (pourcent == null) return Window.getInstance().width;
        return Window.getInstance().width*pourcent;
    }
    // return pourcent of screen height rounded to int
    public static inline function inth(?pourcent:Float):Int {
        if (pourcent == null) return Std.int(Window.getInstance().width);
        return  Std.int(Window.getInstance().height*pourcent);
    }
    // return pourcent of screen width rounded to int
    public static inline function intw(?pourcent:Float):Int {
        if (pourcent == null) return Std.int(Window.getInstance().width);
        return Std.int(Window.getInstance().width*pourcent);
    }


}