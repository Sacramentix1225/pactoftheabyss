package dual;

import h2d.Object;
import hxd.Res;
import h2d.col.Point;
import h2d.col.Polygon;
import hxd.Timer;
import hxd.Key in K;
import haxe.MainLoop;
import haxe.MainLoop.MainEvent;
import h3d.Matrix;
import h3d.Vector;
import scene.sound.SoundManager;
import dual.pyke.Key.State;
import dual.pyke.Spell;
import motion.MotionPath;
import motion.Actuate;
import motion.easing.Linear;
import h2d.col.Bounds;
import hxd.Window;
import h2d.Bitmap;
import h2d.Anim;
import h2d.Tile;
import dual.pyke.Cooldown;
import dual.pyke.Stat;
import dual.pyke.Item;
import vfx.particle.FlashFx;
import dual.tools.ScreenTool as Screen;
using dual.tools.ScreenTool;

import pattern.Observer.NotifPack;

using pattern.Observer.Observables;

class Pyke extends Bitmap {

    public var lastTimeStamp:Float;
    public var updateEvent:MainEvent;
    

    public var stat:Stat;
    public var cooldown:Cooldown;
    public var itemList:Array<Item>;
    public var position:ArenaPosition;
    
    public var gold(default, set) :Int;
    public var win:Int;

    public var hp(default, set) :Float;
    public var currentLive(default, set) :Int;
    public var grabCharge(default, set):Float;
    public var grabFinalSpeed:Float;

    public var body:Object;
    public var harpoon:Object;
    public var harpoonBitmap:Bitmap;
    public var bodyBitmap:Bitmap;

    public var isGrabbing:Bool;
    public var isCharging:Bool;
    public var isDashing:Bool;
    public var isInvinsible:Bool;
    public var isUlting:Bool;
    public var isAnimUlt:Bool;

    public var keyBind:dual.pyke.Key;


    public function new(pos:ArenaPosition, stat:Stat, cooldown:Cooldown, gold:Int, keyBind:dual.pyke.Key) {
        super();
        this.position = pos;
        this.stat = stat;
        this.cooldown = cooldown;
        currentLive = this.stat.live;
        this.hp = this.stat.maxHP;
        this.keyBind = keyBind;
        this.win = 0;
        trace(color);
        
        body = new Object(this);
        var bodyTile = Tile.fromBitmap(Res.pyke.body.toBitmap());
        bodyTile.scaleToSize( hitboxSize(), hitboxSize()/400*293);
        bodyTile.setCenterRatio();
        bodyBitmap = new Bitmap(bodyTile, body);
        
        
        harpoon = new Object(this);
        var harpoonTile = Tile.fromBitmap(Res.pyke.harpoon.toBitmap());
        harpoonTile.scaleToSize(hitboxSize()/3*3.83, hitboxSize()/3 );
        harpoonBitmap = new Bitmap(harpoonTile, harpoon);
        harpoonBitmap.tile.setCenterRatio(0.1,0.5);
        harpoon.y = -Std.int(hitboxSize()/9);
        harpoon.x = -Std.int(hitboxSize()/3);
        harpoon.rotation = 2.705;
        
        Actuate.tween(harpoon, 1.2, {rotation: 0.436}).repeat().reflect().ease(Linear.easeNone);
        addChild(harpoon);
        itemList = new Array<Item>();
        itemList = [for(i in 0...6) ItemFactory.create(Item.none)];
        lastTimeStamp = haxe.Timer.stamp();
        updateEvent = MainLoop.add(update);
        
    }
    public inline function hitboxSize():Float {
        return Screen.h()*stat.hitboxSize;
    }

    public function set_grabCharge(c:Float) {
        var m:Float;
        if (c > stat.maxGrabCharge) m = stat.maxGrabCharge;
        else m=c;
	    return grabCharge = m;
    }
    public function set_hp(d:Float) {
        var r = if (d<0) 0 else if (d> stat.maxHP) stat.maxHP else d;
	    this.notify(new NotifPack(r, hpID));
	    return hp = r;
    }

    public function set_gold(d:Int) {
	    this.notify(new NotifPack(d, goldID));
	    return gold = d;
    }
    
    public function set_currentLive(d:Int) {
        var r = if (d<0) 0 else d;
	    this.notify(new NotifPack(r));
	    return currentLive = r;
    }

    public function getHarpoonHitbox():Polygon {
        var trueHarpoonHitbox = new Polygon();
            trueHarpoonHitbox.push(harpoon.localToGlobal( new Point(0,-hitboxSize()/3*0.5) ));
            trueHarpoonHitbox.push(harpoon.localToGlobal( new Point(0,hitboxSize()/3*0.5) ));
            trueHarpoonHitbox.push(harpoon.localToGlobal( new Point(hitboxSize()/3*2.5, hitboxSize()/3*0.5) ));
            trueHarpoonHitbox.push(harpoon.localToGlobal( new Point(hitboxSize()/3*2.5, -hitboxSize()/3*0.5) ));
            /*trueHarpoonHitbox.push( new Point( harpoon.absX-Window.getInstance().height*this.stat.hitboxSize/3*0.5, harpoon.absY ) );
            trueHarpoonHitbox.push( new Point( harpoon.absX+Window.getInstance().height*this.stat.hitboxSize/3*0.5, harpoon.absY ) );
            trueHarpoonHitbox.push( new Point( harpoon.absX-Window.getInstance().height*this.stat.hitboxSize/3*0.5, harpoon.absY-Window.getInstance().height*this.stat.hitboxSize/3*2.5 ) );
            trueHarpoonHitbox.push( new Point( harpoon.absX+Window.getInstance().height*this.stat.hitboxSize/3*0.5, harpoon.absY-Window.getInstance().height*this.stat.hitboxSize/3*2.5 ) );*/
        return trueHarpoonHitbox;
    }

    public function checkWall(arenaWidth:Float) {
        if ( x < arenaWidth*0.05 ) {
            x = arenaWidth*0.051;
            stat.moveSpeed = Math.abs(stat.moveSpeed);
        }
        if ( x > arenaWidth * 0.95 ) {
            x = arenaWidth*0.949;
            stat.moveSpeed = -Math.abs(stat.moveSpeed);
        }
    }

    public function reveal() {
        if (alpha == 0.1) { 
            cooldown.ghostUsed();
            alpha = 1;
            checkTriggerCondition(TriggerCondition.GhostEnd);
            cooldown.ghostTime*(1-stat.cdr);
        }
    }

    public function swapDirection() {
        stat.moveSpeed *= -1;
    }

    public function chargeGrab(dt:Float) {
        if (cooldown.isGrabReady) {
            reveal();
            isCharging = true;
            grabCharge += dt;
        }
    }

    public function grab() {
        if (cooldown.isGrabReady) {
            isCharging = false;
            Actuate.pause(harpoon);
            Actuate.pause(harpoon.getBounds().rotate);
            grabFinalSpeed = grabCharge*stat.grabSpeed*10;
            grabCharge = stat.beginGrabCharge;
            isGrabbing = true;
            cooldown.grabUsed();
            cooldown.grabTime*(1-stat.cdr);
        }

    }

    public function ghost() {
        if (cooldown.isGhostReady) {
            alpha = 0.1; // should be 0 
            Actuate.timer(BattleArena.t(5)).onComplete(
                function () {
                    reveal();
                }
            );
        }
    }
    

    public function dash() {
        if (cooldown.isDashReady) {
            reveal();
            isDashing=true;
            if (stat.moveSpeed < 0) Actuate.tween(this, BattleArena.t(0.15), {x: x-Window.getInstance().width*0.25}).ease(Linear.easeNone).onComplete(noDash);
            else if (stat.moveSpeed > 0) Actuate.tween(this, BattleArena.t(0.15), {x: x+Window.getInstance().width*0.25}).ease(Linear.easeNone).onComplete(noDash);
            cooldown.dashUsed();
            cooldown.dashTime*(1-stat.cdr);
        }
        
    }
    public function noDash() {
        isDashing=false;
    }

    public function ult() {
        if (cooldown.isUltReady) {
            reveal();
            isAnimUlt = true;
            Actuate.update(this.setScale, BattleArena.t(0.2), [1], [1.5]);
            Actuate.timer(BattleArena.t(0.2)).onComplete(function () {
                isUlting = true;
            });
            cooldown.ultTime*(1-stat.cdr);
        }
    }

    public function flash() {
        if (cooldown.isFlashReady) {
            var flashFx = FlashFx.depart(Game.currentScene, localToGlobal());
            var flashFxOnPyke = FlashFx.onPyke(this);
            if (stat.moveSpeed < 0) x-=Window.getInstance().width*0.25;
            else if (stat.moveSpeed > 0) x+=Window.getInstance().width*0.25;
            var soundFX = if( hxd.res.Sound.supportedFormat(Mp3) || hxd.res.Sound.supportedFormat(OggVorbis) ) hxd.Res.sound.spell.flash else null;
            if (soundFX != null) {
                SoundManager.getInstance().trigger(soundFX);
            }
            
            checkTriggerCondition(TriggerCondition.Flash);
            cooldown.flashUsed();
            cooldown.flashTime*(1-stat.cdr);
        }
    }

    public function respawn() {
        hp = stat.maxHP;
        // make a function for invicibility or powerfull heal on duration
        if (this.position == Top) x = Window.getInstance().width*0.25;
        else x = Window.getInstance().width*0.75;
        
    }

    public function hitAnim(i:Float) {
        var t = new Vector(i,0.31,0.31);
        bodyBitmap.color = t;
    }
    
    /*public static function defaultNew(keyBind:dual.pyke.Key):Pyke {
        var cd = new Cooldown(45, 5, 12, 9, 30);
        return new Pyke(150, 0.09, 2000, 150, 300, 0, cd, 3, 150, 0.2, 1, keyBind);
    }*/

    public static function fromConfig(keybindTop:dual.pyke.Key, keybindBot:dual.pyke.Key):Array<Pyke> {
        var result = new Array<Pyke>();
        var gm = GamemodeConfig.getCurrent();
        var topCd = new Cooldown(gm.topPyke.cooldown.flashCd, gm.topPyke.cooldown.grabCd, gm.topPyke.cooldown.ghostCd, gm.topPyke.cooldown.dashCd, gm.topPyke.cooldown.ultCd);
        var botCd = new Cooldown(gm.botPyke.cooldown.flashCd, gm.botPyke.cooldown.grabCd, gm.botPyke.cooldown.ghostCd, gm.botPyke.cooldown.dashCd, gm.botPyke.cooldown.ultCd);
        var topStat = new Stat(gm.topPyke.stat.moveSpeed, gm.topPyke.stat.hitboxSize, gm.topPyke.stat.maxHP, gm.topPyke.stat.damage, gm.topPyke.stat.flatDamage, gm.topPyke.stat.ultDmgMultiplier, gm.topPyke.stat.grabDmgMultiplier, gm.topPyke.stat.cdr, gm.topPyke.stat.live, gm.topPyke.stat.grabSpeed, gm.topPyke.stat.beginGrabCharge, gm.topPyke.stat.maxGrabCharge);
        var botStat = new Stat(gm.botPyke.stat.moveSpeed, gm.botPyke.stat.hitboxSize, gm.botPyke.stat.maxHP, gm.botPyke.stat.damage, gm.botPyke.stat.flatDamage, gm.botPyke.stat.ultDmgMultiplier, gm.botPyke.stat.grabDmgMultiplier, gm.botPyke.stat.cdr, gm.botPyke.stat.live, gm.botPyke.stat.grabSpeed, gm.botPyke.stat.beginGrabCharge, gm.botPyke.stat.maxGrabCharge);
        var topPyke = new Pyke(ArenaPosition.Top, topStat, topCd, gm.topPyke.gold, keybindTop);
        var botPyke = new Pyke(ArenaPosition.Bot, botStat, botCd, gm.botPyke.gold, keybindBot);
        result.push(topPyke);
        result.push(botPyke);
        return result;
    }
    public function isFull():Bool {
        for (item in itemList) {
            if(item.id==Item.none) return false;
        }
        return true;
    }

    public function give(item:Item, pos:Int) {
        if (itemList[pos].id==Item.none) itemList[pos] = item;
        else {
            for (item in itemList) {
                if (item.id==Item.none) {
                    item = itemList[pos];
                    itemList[pos] = item;
                }
            }
        }
        recalculateStat();
    }

    public function recalculateStat() {
        var finalStat:Stat;
        var gm = GamemodeConfig.getCurrent();
        if (position == ArenaPosition.Top) {
            finalStat = new Stat(gm.topPyke.stat.moveSpeed, gm.topPyke.stat.hitboxSize, gm.topPyke.stat.maxHP, gm.topPyke.stat.damage, gm.topPyke.stat.flatDamage, gm.topPyke.stat.ultDmgMultiplier, gm.topPyke.stat.grabDmgMultiplier, gm.topPyke.stat.cdr, gm.topPyke.stat.live, gm.topPyke.stat.grabSpeed, gm.topPyke.stat.beginGrabCharge, gm.topPyke.stat.maxGrabCharge);
        } else {
            finalStat = new Stat(gm.botPyke.stat.moveSpeed, gm.botPyke.stat.hitboxSize, gm.botPyke.stat.maxHP, gm.botPyke.stat.damage, gm.botPyke.stat.flatDamage, gm.botPyke.stat.ultDmgMultiplier, gm.botPyke.stat.grabDmgMultiplier, gm.botPyke.stat.cdr, gm.botPyke.stat.live, gm.botPyke.stat.grabSpeed, gm.botPyke.stat.beginGrabCharge, gm.botPyke.stat.maxGrabCharge);
        }
        for (item in itemList) {
            finalStat.add(item.stat);
        }
        stat = finalStat;

        ///bodyBitmap.tile = Tile.fromBitmap(Res.pyke.body.toBitmap());
        bodyBitmap.tile.scaleToSize( hitboxSize(), hitboxSize()/400*293);
        bodyBitmap.tile.setCenterRatio();
         
        //harpoonBitmap.tile = Tile.fromBitmap(Res.pyke.harpoon.toBitmap());
        harpoonBitmap.tile.scaleToSize(hitboxSize()/3*3.83, hitboxSize()/3 );
        harpoonBitmap.tile.setCenterRatio(0.1,0.5);
        

    }

    public function reset() {
        currentLive = stat.live;
        respawn();
    }

    public function update() {
        if (!Round.isShopping) {
            var timeStamp = haxe.Timer.stamp();
            var dt = timeStamp-lastTimeStamp;
            dt *= BattleArena.getInstance().timeFactor;
            lastTimeStamp = timeStamp;
            cooldown.update(dt);
            keyBind.update(dt);
            itemUpdate(dt);


            if(!BattleArena.getInstance().isControlBlocked) {
                if (keyBind.swapDirection) swapDirection();

                if (!isDashing){
                    if (!isCharging) {
                        if (!isGrabbing) {
                            if (!isAnimUlt) {
                                if (keyBind.ghost) ghost();
                                if (keyBind.dash) dash();
                                if (keyBind.ult) ult();
                            }
                        }
                    
                        
                    }
                    if (!isGrabbing) {
                        if (keyBind.grab == State.Charging) chargeGrab(dt);
                        if (keyBind.grab == State.Launch) grab();
                    }
                }
                if (keyBind.flash) flash();
            }
            if ( isGrabbing ) {
                if ( harpoon.isOnYScreen() ) {
                    harpoon.move(grabFinalSpeed*dt, grabFinalSpeed*dt);
                } else {
                    isGrabbing = false;
                    harpoon.y = -Std.int(Window.getInstance().height*0.01);
                    harpoon.x = -Std.int(Window.getInstance().height*0.03);
                    Actuate.resume(harpoon);
                    Actuate.resume(harpoon.getBounds().rotate);
                }
            } else if (isCharging || isAnimUlt) {
                
            } else {
                x += stat.moveSpeed*dt*Window.getInstance().width*0.0007;
            }

            checkWall(Window.getInstance().width);
            x=x;
        }
    }

    public function checkTriggerCondition(trigger:TriggerCondition) {
        for (item in itemList) {
            if (item.type == Passive) {
                if (cast(item, PassiveItem).triggerCondition == trigger) {
                    item.effect(this);
                }
            }
        }
    }

    public function itemUpdate(dt:Float) {
        for ( item in itemList) {
            if (item.type == ItemType.Active) {
                if (K.isReleased(cast(item, ActiveItem).triggerKey)) {
                    item.effect(this);
                }
            }
        }
    }

    public static var goldID:Int = 0;
    public static var hpID:Int = 1;


}