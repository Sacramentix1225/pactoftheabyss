package dual;


import h2d.col.Polygon;
import h3d.Vector;
import h2d.col.Polygon;
import h2d.col.Segment;
import motion.Actuate;
import motion.easing.Linear;
import h2d.col.Point;
import hxd.Window;
import h2d.col.Bounds;
import dual.tools.ScreenTool as Screen;


class Collision {

    public static function checkHitWith(attack:Pyke, defense:Pyke) {
        if (attack.isGrabbing) {
            if ( collidePoly(attack.getHarpoonHitbox(), defense.body.getBounds()) ) {
                defense.hp -= attack.stat.damage*attack.stat.grabDmgMultiplier+attack.stat.flatDamage;
                attack.isGrabbing = false;
                attack.harpoon.y = -Screen.h(0.01);
                attack.harpoon.x = -Screen.h(0.03);
                Actuate.resume(attack.harpoon);
                Actuate.update(defense.hitAnim, BattleArena.t(0.05), [0.5], [1]).repeat(10).reflect().ease(Linear.easeNone).onComplete(function() {
                    defense.bodyBitmap.color = new Vector(1,1,1);
                });
                if (checkHP(attack, defense)) attack.gold += 300;

            }
        }
        

        if (attack.isUlting) {
            attack.isInvinsible = true;
            defense.isInvinsible = true;
            var lastPosition = new Point(attack.x, attack.y);
            Actuate.tween(attack, BattleArena.t(0.15), { y : defense.y});
            Actuate.update(attack.setScale, BattleArena.t(0.2), [attack.scaleX], [1]).onComplete (
                function () {
                    if (attack.body.getBounds().intersects(defense.body.getBounds())) {
                        defense.hp -= attack.stat.damage*attack.stat.ultDmgMultiplier+attack.stat.flatDamage;
                        
                        Actuate.update(defense.hitAnim, BattleArena.t(0.05), [0.5], [1]).repeat(10).reflect().ease(Linear.easeNone).onComplete(function() {
                            defense.bodyBitmap.color = new Vector(1,1,1);
                            attack.isInvinsible = false;
                            defense.isInvinsible = false;
                        });
                        if (checkHP(attack, defense)) attack.gold += 300*2;
                    }
                    Actuate.tween(attack, BattleArena.t(0.2), { x : lastPosition.x});
                    Actuate.tween(attack, BattleArena.t(0.2), { y : lastPosition.y});
                    Actuate.timer(BattleArena.t(0.2)).onComplete(
                        function () {
                            attack.isAnimUlt = false;
                            attack.cooldown.ultUsed();
                        }
                    );
                }
            );
            
            
            attack.isUlting = false;
        }
    }

    public static function checkHP(attack:Pyke, defense:Pyke):Bool {
        var isDead = false;
        if ( defense.hp == 0 ) {
            defense.currentLive--;
            defense.respawn();
            isDead = true;
            if ( defense.currentLive == 0 ) {
                Round.win(attack,defense);
            }
        }
        return isDead;
    }

    public static function collideSeg(seg1:Segment, seg2:Segment):Bool {
        
        var dot = seg1.dx * seg2.dy - seg1.dy * seg2.dx;

        if (dot ==0) return false;

        var c = new Point(seg2.x-seg1.x, seg2.y-seg1.y);
        var t = ( c.x * seg2.dy - c.y * seg2.dx ) / dot;

        if ( t < 0 || t > 1 ) return false;

        var u = ( c.x * seg1.dy - c.y * seg1.dx ) / dot;

        if ( u < 0 || u > 1 ) return false;

        return true;
    }

    public static function collidePoly(poly:Polygon, bounds:Bounds ):Bool {

        //bounds to segments
        var listSeg = new Array<Segment>();
        listSeg.push( new Segment( new Point(bounds.x, bounds.y) ,  new Point(bounds.x+bounds.width, bounds.y+bounds.height) ) );
        listSeg.push( new Segment( new Point(bounds.x+bounds.width, bounds.y) ,  new Point(bounds.x+bounds.width, bounds.y+bounds.height) ) );
        listSeg.push( new Segment( new Point(bounds.x+bounds.width, bounds.y+bounds.height) ,  new Point(bounds.x, bounds.y+bounds.height) ) );
        listSeg.push( new Segment( new Point(bounds.x, bounds.y+bounds.height) ,  new Point(bounds.x, bounds.y) ) );

        for ( ps in poly.toSegments() ) {
            for ( bs in listSeg ) {
                 if (collideSeg(ps, bs)) return true;
            }
        }

        return false;
    }
    

}