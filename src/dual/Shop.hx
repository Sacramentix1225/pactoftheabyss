package dual;

import h3d.mat.Texture;
import h2d.Particles;
import pattern.Observer.Watcher;
import pattern.Observer.NotifPack;
import dual.pyke.Item;
import scene.gameUI.ItemCard;
import h2d.Flow;
import scene.gameUI.ScrollBox;
import scene.gameUI.MoreInfoBox;
import h2d.Text;
import h2d.Tile;
import h2d.Bitmap;
import h2d.Object;
import hxd.Window;
import scene.FontFolder;
import hxd.Window;
import dual.tools.ScreenTool as Screen;

using pattern.Observer.Observables;

class Shop extends Object {
    
    public var background:Bitmap;
    public var title:Text;
    public var topGold:Text;
    public var botGold:Text;
    public var topScrollBox:ScrollBox;
    public var botScrollBox:ScrollBox;
    public var botMoreInfoBox:MoreInfoBox;
    public var topMoreInfoBox:MoreInfoBox;

    //public var particles:Particles;
    //public var buyParticleGroup:ParticleGroup;
    


    public function new(?parent:Object) {
        super(parent);
        

        var bgTile = Tile.fromColor(0x161616);
        bgTile.scaleToSize(Window.getInstance().width, Window.getInstance().height);
        background = new Bitmap(bgTile, this);
        title = new h2d.Text(FontFolder.pirata64, this);
        title.textColor = 0xD4AF37;
        title.text ="SHOP";
        title.maxWidth = Window.getInstance().width;
        title.textAlign = Center;
        title.x = 0;
        title.y = 0; // Change this if you change the font for good alignement
        title.smooth = true;
/*
        topGold = new Text(FontFolder.pirata16);
        topGold.textAlign = Right;
        topGold.x = Screen.w()-5;
        topGold.y = Screen.h()-5-topGold.textHeight;
        topGold.text = Std.string(BattleArena.getInstance().topPyke.gold);
        topGold.textColor = 0xd4af37;
        var callBack = function(notif:NotifPack):Any {
                if (notif.type == Pyke.goldID) topGold.text = Std.string(notif.data);
                return null;
            }
        var goldw2 = new Watcher("Gold", callBack);
        BattleArena.getInstance().topPyke.addObserver(goldw2);
        
        botGold = new Text(FontFolder.pirata16);
        botGold.textAlign = Right;
        botGold.x = Screen.w()-5;
        botGold.y = Screen.h()-5-botGold.textHeight;
        botGold.text = Std.string(BattleArena.getInstance().botPyke.gold);
        botGold.textColor = 0xd4af37;
        var callBack = function(notif:NotifPack):Any {
                if (notif.type == Pyke.goldID) botGold.text = Std.string(notif.data);
                return null;
            }
        var goldw2 = new Watcher("Gold", callBack);
        BattleArena.getInstance().botPyke.addObserver(goldw2);
*/
        var topList = new Array<Flow>();
        var botList = new Array<Flow>();
        for (i in 1...7) {
            var info = ItemFactory.getItemInfo(i);
            var itemCard = new ItemCard(FontFolder.pirata16, info.name + ": " + info.quickDesc, info.value+"g", i);
            topList.push(itemCard);
            itemCard = new ItemCard(FontFolder.pirata16, info.name + ": " + info.quickDesc, info.value+"gg", i); //DEBUG
            topList.push(itemCard);
        }

        for (i in 1...7) {
            var info = ItemFactory.getItemInfo(i);
            var itemCard = new ItemCard(FontFolder.pirata16, info.name + ": " + info.quickDesc, info.value+"g", i);
            botList.push(itemCard);
            itemCard = new ItemCard(FontFolder.pirata16, info.name + ": " + info.quickDesc, info.value+"gg", i);
            botList.push(itemCard);
        }

        topScrollBox = new ScrollBox(Screen.intw(0.48), roundToEntireBox(), roundToEntireBox(true), topList, BattleArena.getInstance().topPyke);
        topScrollBox.y = title.textHeight+5;
        addChild(topScrollBox);

        

        botScrollBox = new ScrollBox(Screen.intw(0.48), roundToEntireBox(), roundToEntireBox(true), botList, BattleArena.getInstance().botPyke);
        botScrollBox.y = title.textHeight+5;
        botScrollBox.x = Screen.w(0.52);
        addChild(botScrollBox);

        var moreInfoHeight = Screen.h()-(title.textHeight+5)-roundToEntireBox()-5;

        topMoreInfoBox = new MoreInfoBox(Screen.w(0.48), moreInfoHeight, cast(topScrollBox.flowBox.getChildAt(topScrollBox.selected), ItemCard).itemID);
        topMoreInfoBox.y = (title.textHeight+5)+roundToEntireBox()+5;
        addChild(topMoreInfoBox);

        var callBackTop = function(notif:NotifPack):Any {
                topMoreInfoBox.onSelectionChange(Std.int(notif.data));
                return null;
            }
        var topw = new Watcher("moreInfo", callBackTop);
        topScrollBox.addObserver(topw);
        
        botMoreInfoBox = new MoreInfoBox(Screen.w(0.48), moreInfoHeight, cast(botScrollBox.flowBox.getChildAt(botScrollBox.selected), ItemCard).itemID);
        botMoreInfoBox.y = (title.textHeight+5)+roundToEntireBox()+5;
        botMoreInfoBox.x = Screen.w(0.52);
        addChild(botMoreInfoBox);

        var callBackBot = function(notif:NotifPack):Any {
                botMoreInfoBox.onSelectionChange(Std.int(notif.data));
                return null;
            }
        var botw = new Watcher("moreInfo", callBackBot);
        botScrollBox.addObserver(botw);
        
    }

    private function roundToEntireBox(getBox = false):Int {
        var height = Screen.h(0.48);
        var roundHeight = 5;
        var i = 0;
        while ((roundHeight+55) <= height) {
            roundHeight += 55;
            i++;
        }
        trace(height + " --> " + roundHeight);
        if (getBox) {
            return i;
        }
        return roundHeight;
    }

    public function update(dt:Float) {
        topScrollBox.update(dt);
        botScrollBox.update(dt);
    }

}