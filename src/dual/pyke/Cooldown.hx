package dual.pyke;

import pattern.Observer.NotifPack;
import dual.pyke.Spell;

using pattern.Observer.Observables;

class Cooldown {
	public var flashCd:Float;
	public var flashTime(default, set):Float;
	public var isFlashReady:Bool;

	public var grabCd:Float;
	public var grabTime(default, set):Float;
	public var isGrabReady:Bool;

	public var ghostCd:Float;
	public var ghostTime(default, set):Float;
	public var isGhostReady:Bool;

	public var dashCd:Float;
	public var dashTime(default, set):Float;
	public var isDashReady:Bool;

	public var ultCd:Float;
	public var ultTime(default, set):Float;
	public var isUltReady:Bool;

	public function new(flashCd:Float, grabCd:Float, ghostCd:Float, dashCd:Float, ultCd:Float) {
		this.flashCd = flashCd;
        this.flashTime = 0;
        this.isFlashReady = true;

		this.grabCd = grabCd;
        this.grabTime = 0;
        this.isGrabReady = true;

		this.ghostCd = ghostCd;
        this.ghostTime = 0;
        this.isGhostReady = true;

		this.dashCd = dashCd;
        this.dashTime = 0;
        this.isDashReady = true;

		this.ultCd = ultCd;
        this.ultTime = 0;
        this.isUltReady = true;
        
	}

    public function set_flashTime(m:Float) {
        var r:Float;
        if (m < 0) {
            r=0;
            isFlashReady = true;
        }
        else r=m;
        this.notify(new NotifPack(r,Spell.flash));
        return flashTime = r;
    }

    public function set_grabTime(m:Float) {
        var r:Float;
        if (m < 0) {
            r=0;
            isGrabReady = true;
        }
        else r=m;
        this.notify(new NotifPack(r,Spell.grab));
        return grabTime = r;
    }

    public function set_ghostTime(m:Float) {
        var r:Float;
        if (m < 0) {
            r=0;
            isGhostReady = true;
        }
        else r=m;
        this.notify(new NotifPack(r,Spell.ghost));
        return ghostTime = r;
    }

    public function set_dashTime(m:Float) {
        var r:Float;
        if (m < 0) {
            r=0;
            isDashReady = true;
        }
        else r=m;
        this.notify(new NotifPack(r,Spell.dash));
        return dashTime = r;
    }

    public function set_ultTime(m:Float) {
        var r:Float;
        if (m < 0) {
            r=0;
            isUltReady = true;
        }
        else r=m;
        this.notify(new NotifPack(r,Spell.ult));
        return ultTime = r;
    }

    public function flashUsed() {
        isFlashReady = false;
        flashTime = flashCd;
    }
    public function grabUsed() {
        isGrabReady = false;
        grabTime = grabCd;
    }
    public function ghostUsed() {
        isGhostReady = false;
        ghostTime = ghostCd;
    }
    public function dashUsed() {
        isDashReady = false;
        dashTime = dashCd;
    }
    public function ultUsed() {
        isUltReady = false;
        ultTime = ultCd;
    }

    public function updateCdr(previous:Float, now:Float) {
        var deltaCdr = now - previous;
        grabTime *= (1-deltaCdr);
        ghostTime *= (1-deltaCdr);
        dashTime *= (1-deltaCdr);
        ultTime *= (1-deltaCdr);
        flashTime *= (1-deltaCdr);
    }

	public function update(dt:Float) {
        if (!isFlashReady) {
            flashTime -= dt;
        }
        if (!isGrabReady) {
            grabTime -= dt;
        }
        if (!isGhostReady) {
            ghostTime -= dt;
        }
        if (!isDashReady) {
            dashTime -= dt;
        }
        if (!isUltReady) {
            ultTime -= dt;
        }
    }
}

