package dual.pyke;

import hxd.Key in K;

class Key {
    public var swapDirection:Bool;
    public var grab:State;
    public var ghost:Bool;
    public var dash:Bool;
    public var ult:Bool;
    public var flash:Bool;

    public var swapDirectionKey:Int;
    public var grabKey:Int;
    public var ghostKey:Int;
    public var dashKey:Int;
    public var ultKey:Int;
    public var flashKey:Int;
    public var itemKey:Array<Int>;

    public function new(swapDirection:Int, grab:Int, ghost:Int, dash:Int, ult:Int, flash:Int, item:Array<Int>) {
        swapDirectionKey = swapDirection;
        grabKey = grab;
        ghostKey = ghost;
        dashKey = dash;
        ultKey = ult;
        flashKey = flash;
        itemKey = item;
    }

    public function update(dt:Float) {
        swapDirection = K.isReleased(swapDirectionKey);
        if ( K.isDown(grabKey) ) grab = State.Charging;
        else if ( K.isReleased(grabKey) ) grab = State.Launch;
        else grab = State.None;
        ghost = K.isReleased(ghostKey);
        dash = K.isReleased(dashKey);
        ult = K.isReleased(ultKey);
        flash = K.isReleased(flashKey);
        
    }

    

}

enum State {
        None;
        Charging;
        Launch;
    }