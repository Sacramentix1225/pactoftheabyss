package dual.pyke;

import pattern.Observer.NotifPack;
using pattern.Observer.Observables;
class Stat {
    
    public var moveSpeed:Float;
    public var hitboxSize:Float;
    public var maxHP:Float;
    public var damage:Float;
    public var flatDamage:Float; //not multiplied
    public var ultDmgMultiplier:Float;
    public var grabDmgMultiplier:Float;
    public var cdr(default, set) :Float;
    public var live:Int;
    public var grabSpeed:Float;
    public var beginGrabCharge:Float;
    public var maxGrabCharge:Float;


    public function new(moveSpeed:Float, hitboxSize:Float, maxHP:Float, damage:Float, flatDamage:Float, ultDmgMultiplier:Float, grabDmgMultiplier:Float,  cdr:Float, live:Int, grabSpeed:Float, beginGrabCharge:Float, maxGrabCharge:Float) {
        this.moveSpeed = moveSpeed;
        this.hitboxSize = hitboxSize;
        this.maxHP = maxHP;
        this.damage = damage;
        this.flatDamage = flatDamage;
        this.ultDmgMultiplier = ultDmgMultiplier;
        this.grabDmgMultiplier = grabDmgMultiplier;
        this.cdr = cdr;
        this.live = live;
        this.grabSpeed = grabSpeed;
        this.beginGrabCharge = beginGrabCharge;
        this.maxGrabCharge = maxGrabCharge;

    }
    public function add(addedStat:Stat) {
        this.moveSpeed += addedStat.moveSpeed;
        this.hitboxSize += addedStat.hitboxSize;
        this.maxHP += addedStat.maxHP;
        this.damage += addedStat.damage;
        this.flatDamage += addedStat.flatDamage;
        this.ultDmgMultiplier += addedStat.ultDmgMultiplier;
        this.grabDmgMultiplier += addedStat.grabDmgMultiplier;
        this.cdr += addedStat.cdr;
        this.live += addedStat.live;
        this.grabSpeed += addedStat.grabSpeed;
        this.beginGrabCharge += addedStat.beginGrabCharge;
        this.maxGrabCharge += addedStat.maxGrabCharge;
    }

    public function set_cdr(d:Float) {
        var r = if (d>0.45) 0.45 else d;
	    this.notify(new NotifPack(r));
	    return cdr = r;
    }

}