package dual.pyke;

import motion.easing.Linear;
import motion.Actuate;
import dual.pyke.Stat;
import dual.ArenaPosition;

enum ItemType {
    Active;
    Passive;
    None;
}
typedef ItemInfo = {
        var value:Int;
        var stat:String;
        var name:String;
        var quickDesc:String;
        var effect:String;
        var lore:String;
        var type:String;
}


class ItemFactory {
    //TODO finish this
    public static function getItemInfo(itemID:Int):ItemInfo {
        if (itemID==Item.duskblade) {
            var value = 700;
            var name = "Notched Dagger of the Lost Crewman";
            var quickDesc = "extra dmg on stealth exit";
            var stat = "+35Dmg, +10RawDmg, +25GrabSpeed, +20%BeginGrabCharge";
            var effect = "+200RawDmg on stealth exit for 5s. 35s CD";
            var lore = "No one really knows who this dagger belonged to, but some rumours say that the owner always attack in the darkness of the mist.";
            var type = "passive";
            var itemInfo:ItemInfo = {value: value, stat: stat, name: name, effect: effect, type: type, quickDesc: quickDesc, lore: lore}
            return itemInfo;
        } else if (itemID==Item.pact) {
            var value = 500;
            var name = "The Abyssal Pact";
            var quickDesc = "x3 grab damage after flash";
            var stat = "-25ms, get bigger, +25Dmg, +10RawDmg";
            var effect = "x3 grab damage for 1.5s after flash";
            var lore = "Choose to follow the Abyssal Pact at the cost of your freedom and the blacklisted one's will pay at the cost of their life.";
            var type = "passive";
            var itemInfo:ItemInfo = {value: value, stat: stat, name: name, effect: effect, type: type, quickDesc: quickDesc, lore: lore}
            return itemInfo;
        } else if (itemID==Item.shojin) {
            var value = 950;
            var name = "maelstörm spear";
            var quickDesc = "+20%CDR on ult for 10s";
            var stat = "+10Dmg, +10RawDmg, +10% ultDmg, +10%CDR, +25grabSpeed, +20%BeginGrabCharge, +10%MaxGrabCharge";
            var effect = "+20%CDR on ult for 10s";
            var lore = "Once a time, Karma was the reincarnation of Cryll Don a sailor under the pavillon of a monster hunter pirate ship. But Cryll Don did not agree with the violence that the captain was showing to the member who was commiting mistakes. The past memories of Karma was comming back and lead Cryll Don to unleash the power of maelstörm and sink his own ship to restore the peace.";
            var type = "passive";
            var itemInfo:ItemInfo = {value: value, stat: stat, name: name, effect: effect, type: type, quickDesc: quickDesc, lore: lore}
            return itemInfo;
        } else if (itemID==Item.boot) {
            var value = 200;
            var name = "boot.";
            var quickDesc = "no effect; +40ms, +25grabSpeed";
            var stat = "+40Ms, +25grabSpeed";
            var effect = "none";
            var lore = "WAIT ! You expected some story about basic boot go check the wikipedia page about boot.";
            var type = "stat";
            var itemInfo:ItemInfo = {value: value, stat: stat, name: name, effect: effect, type: type, quickDesc: quickDesc, lore: lore}
            return itemInfo;
        } else if (itemID==Item.potion) {
            var value = 100;
            var name = "red potion";
            var quickDesc = "heal 300 hp";
            var stat = "none";
            var lore = "No one really know of what this potion is made, but everyone love it !";
            var effect = "heal";
            var type = "stat";
            var itemInfo:ItemInfo = {value: value, stat: stat, name: name, effect: effect, type: type, quickDesc: quickDesc, lore: lore}
            return itemInfo;
        } else if (itemID==Item.taunt) {
            var value = 550;
            var name = "EZ taunt";
            var quickDesc = "swap direction of ennemy pyke on trigger.";
            var stat = "+35RawDmg, +10%CDR, +75grabSpeed, +30%MaxGrabCharge";
            var effect = "swap direction of ennemy pyke on trigger. 15s CD";
            var lore = "pierrem's job";
            var type = "active";
            var itemInfo:ItemInfo = {value: value, stat: stat, name: name, effect: effect, type: type, quickDesc: quickDesc, lore: lore}
            return itemInfo;
        }

        return null;
    }
    
    public static function create(itemID:Int):Item {
        if (itemID==Item.none) {

            var stat = new Stat(0,0,0,0,0,0,0,0,0,0,0,0);
            var item = new Item(Item.none, stat);
            return item;
        } if (itemID==Item.duskblade) {
            var effect = function (pyke:Pyke) {
                pyke.stat.flatDamage += 200;
                Actuate.timer(5).onComplete(function () {pyke.stat.flatDamage -= 200;});
            }
            var stat = new Stat(0,0,0,35,10,0,0,0,0,25,0.2,0);
            var item = new PassiveItem(Item.duskblade, stat, 0, effect, TriggerCondition.GhostEnd);
            return item;
        } if (itemID==Item.pact) {
            var effect = function (pyke:Pyke) {
                pyke.stat.grabDmgMultiplier += 2;
                Actuate.timer(5).onComplete(function () {pyke.stat.grabDmgMultiplier -= 2;});
            }
            var stat = new Stat(-25,0.02,0,25,10,0,0,0,0,0,0,0);
            var item = new PassiveItem(Item.pact, stat, 0, effect, TriggerCondition.Flash);
            return item;

        } if (itemID==Item.shojin) {
            var effect = function (pyke:Pyke) {
                var previousCdr = pyke.stat.cdr;
                pyke.stat.cdr += 0.2;
                pyke.cooldown.updateCdr(previousCdr, pyke.stat.cdr);
                Actuate.timer(10).onComplete(function () {
                    pyke.stat.cdr = previousCdr;
                    pyke.cooldown.updateCdr(pyke.stat.cdr, previousCdr);
                });
            }
            var stat = new Stat(0,0,0,10,10,0.1,0,0.1,0,25,0.3,0.1);
            var item = new PassiveItem(Item.shojin, stat, 0, effect, TriggerCondition.UltOnHit);
            return item;

        } if (itemID==Item.boot) {
            
            var stat = new Stat(40,0,0,0,0,0,0,0,0,25,0,0);
            var item = new Item(Item.boot, stat);
            return item;

        } if (itemID==Item.potion) {
            var item:Item;
            var effect = function (pyke:Pyke) {
                Actuate.tween(pyke, 5, {hp : pyke.hp+300}).ease(Linear.easeNone).onComplete(
                    function () {
                        Item.destroy(pyke, item);
                    }
                );
                
            }
            var stat = new Stat(0,0,0,0,0,0,0,0,0,0,0,0);
            item = new ActiveItem(Item.potion, stat, 0, effect, 0);
            return item;

        } if (itemID==Item.taunt) {
           var effect = function (pyke:Pyke) {
                var target:Pyke;
                if (pyke.position == ArenaPosition.Top) {
                    target = BattleArena.getInstance().botPyke;
                } else {
                    target = BattleArena.getInstance().topPyke;
                }
                target.swapDirection();
            };
            var stat = new Stat(0,-0.02,0,0,35,0,0,0.1,0,75,0,0.3);
            var item = new ActiveItem(Item.taunt, stat, 0, effect, 0);
            return item;

        } else {return null;}
    }

}

class Item {

    public static var none:Int = 0;
    public static var duskblade:Int = 1;
    public static var pact:Int = 2;
    public static var shojin:Int = 3;
    public static var boot:Int = 4;
    public static var potion:Int = 5;
    public static var taunt:Int = 6;

    public var id:Int;
    public var stat:Stat;
    public var cd:Float;
    public var isReady:Bool;
    public var data:Any;
    public var type:ItemType;

    public dynamic function effect(pyke:Pyke) {

	}
    public static function destroy(pyke:Pyke, item:Item) {
        pyke.itemList.remove(item);
        item = null;
    }

    public function new(id:Int, stat:Stat) {
        this.id = id;
        this.stat = stat;
        type = None;
    }

}

class ActiveItem extends Item  {
    
    public var triggerKey:Int; // hxd.key
    
    public function new(id:Int, stat:Stat, cd:Float, effect:Pyke->Void, triggerKey:Int) {
        super(id, stat);
        this.cd = cd;
        this.effect = effect;
        this.triggerKey = triggerKey;
        this.type = Active;

    }
}

class PassiveItem extends Item  {
    
    public var triggerCondition:TriggerCondition; // hxd.key
    
    public function new(id:Int, stat:Stat, cd:Float, effect:Pyke->Void, triggerCondition:TriggerCondition) {
        super(id, stat);
        this.cd = cd;
        this.effect = effect;
        this.triggerCondition = triggerCondition;
        this.type = Passive;

    }
}

enum TriggerCondition {
    Flash;
    IsCharging;
    IsGrabbing;
    Dash;
    Dashing;
    Ult;
    UltOnHit;
    Ghost;
    GhostEnd;
    Win;
}



