package dual;
#if !js
import sys.io.FileInput;
import sys.io.FileOutput;
import sys.FileSystem;
import sys.io.File;
import haxe.io.BytesOutput;
import haxe.io.Bytes;
#end
typedef Gamemode = {
        var topPyke:Player;
        var botPyke:Player;
        var round:Int;
        var name:String;
        var fairplayGold:Int;

    }

    typedef Player = {

        var stat:Stat;
        var gold:Int;
        var cooldown:Cd;

    }

    typedef Stat = {

        var hitboxSize:Float;
        var moveSpeed:Float;
        var maxHP:Float;
        var damage:Float;
        var flatDamage:Float;
        var ultDmgMultiplier:Float;
        var grabDmgMultiplier:Float;
        var cdr:Float;
        var grabSpeed:Float;
        var beginGrabCharge:Float;
        var maxGrabCharge:Float;
        var live:Int;

    }

    typedef Cd = {
        var grabCd:Float;
        var ghostCd:Float;
        var dashCd:Float;
        var ultCd:Float;
        var flashCd:Float;
    }


class GamemodeConfig {


    public static var list:Array<Gamemode>;
    public static var currentGamemodeID:Int = 0;

    #if !js
    public static function fromFolder() {
        list = new Array<Gamemode>();
        for (file in FileSystem.readDirectory("gamemode")) {
			var json = File.read("gamemode\\" + file);
            var data = json.readAll();
            var gamemode:Gamemode = haxe.Json.parse(data.getString(0,data.length));
            list.push(gamemode);
        }
    }
    #end

    #if js 
    public static function fromFolder() {
        list = new Array<Gamemode>();
        var stat:Stat = {
            beginGrabCharge:0.2,
            cdr:0,
            damage:150,
            flatDamage:0,
            grabDmgMultiplier:1,
            grabSpeed:150,
            hitboxSize:0.09,
            live:1,
            maxGrabCharge:1,
            maxHP:2000,
            moveSpeed:150,
            ultDmgMultiplier:3.5
        };
        var cd:Cd = {
            dashCd:9,
            flashCd:45,
            ghostCd:12,
            grabCd:5,
            ultCd:0
        };
        var top:Player = {
            cooldown:cd,
            stat:stat,
            gold:0
        };
        var bot:Player = {
            cooldown:cd,
            stat:stat,
            gold:0
        };
        var gamemode:Gamemode = {
            topPyke:top,
            botPyke:top,
            round:3,
            name:"fairplay",
            fairplayGold:300
        };
        list.push(gamemode);

    }
    #end

    public static function getCurrent():Gamemode {
        return list[currentGamemodeID];
    }


}