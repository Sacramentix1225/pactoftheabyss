package dual;

import pattern.Observer.NotifPack;
import motion.Actuate;
import h3d.pass.Shadows;
import dual.Shop;
import haxe.MainLoop;
using pattern.Observer.Observables;

class Round {

    public static var shopInstance:Shop;

    public static var isShopping (default, set):Bool = false;

    public static function set_isShopping(b:Bool):Bool {
        shopInstance.notify(new NotifPack(b,0));
        return b;
    }

    public static function win(winner:Pyke, loser:Pyke) {
        winner.win++;
        if ( winner.win == GamemodeConfig.getCurrent().round) {
            finish();
        } else {
            Actuate.pause(BattleArena.getInstance().topPyke.harpoon);
            Actuate.pause(BattleArena.getInstance().botPyke.harpoon);
            Actuate.tween(BattleArena.getInstance(), 3.5, {timeFactor : 0.3}).onComplete(shop);

        }
    }

    public static function shop() {
        //init a new scene shop
        isShopping = true;
        BattleArena.getInstance().isControlBlocked = true;

        shopInstance = new Shop(BattleArena.getInstance());
        Actuate.timer(30).onComplete(function () {
            Actuate.tween(BattleArena.getInstance(), 3.5, {timeFactor : 1}).onComplete(function () {
                Actuate.resume(BattleArena.getInstance().topPyke.harpoon);
                Actuate.resume(BattleArena.getInstance().botPyke.harpoon);
                BattleArena.getInstance().isControlBlocked = false;
            });
            isShopping = false;
            BattleArena.getInstance().lastTimeStamp = haxe.Timer.stamp();
            BattleArena.getInstance().topPyke.lastTimeStamp = haxe.Timer.stamp();
            BattleArena.getInstance().botPyke.lastTimeStamp = haxe.Timer.stamp();
            Actuate.tween(shopInstance,1, {alpha : 0}).onComplete(function () {
                BattleArena.getInstance().removeChild(shopInstance);
                shopInstance = null;
                
                
            });
        });

        

    }

    public static function finish() {
        //init a new scene of recap of the game
    }

    public static function restart() {
        //full heal all pyke, restore live and respawn them!
        BattleArena.getInstance().topPyke.reset();
        BattleArena.getInstance().botPyke.reset();
    }

}